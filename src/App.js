import React from 'react';
//import logo from './logo.svg';
import Toolbar from "@material-ui/core/Toolbar"
import './App.css';
import { AppBar, Button, Avatar } from '@material-ui/core';
import MapContainter from './Components/MapComponent/MapContainer2';
import logo from "./images/ets-logo.PNG";
import Typography from '@material-ui/core/Typography'


const defaultCenter = {
  lat: 54.360131,
  lng: 18.645343
}

var point = defaultCenter;


var merkersList = [
  {
    id: 1,
    pos: {
      lat: 54.360131,
      lng: 18.641323
    },
    text: "Biuro Blunovation"
  },
  {
    id: 2,
    pos: {
      lat: 54.360133,
      lng: 18.645342
    },
    text: "nowy merker"
  }
]

function clickHandler(e) {
  point = defaultCenter;
  console.log(point);
}

const buttonStyle = {
  marginLeft: 15
}

function App() {
  return (
    <div>
      <AppBar color="default" position="static">
        <Toolbar>
          <Avatar
          style={{ backgroundColor: "white", marginRight: 5 }}
            alt="Remy Sharp"
            src={logo} />
            <Typography variant="h6">ETS Polska</Typography>
            <div style={{marginLeft: "50px"}}>
              <Button variant="contained">Mapa</Button>
              <Button style={buttonStyle} variant="outlined">Raporty</Button>
              <Button style={buttonStyle} variant="outlined">SMS</Button>
              <Button style={buttonStyle} variant="outlined">Dane</Button>
              <Button style={buttonStyle} variant="outlined">Samochody</Button>
              <Button style={buttonStyle} variant="outlined">Grupy</Button>
              <Button style={buttonStyle} variant="outlined">Ustawienia</Button>
              <Button style={{ position: "absolute", right: 0, marginRight: 15 }} variant="outlined">Wyloguj</Button>
            </div>
        </Toolbar>
      </AppBar>
      <div style={{ height: '800px', width: '80%', float: 'right' }}>
        <MapContainter centerPoint={point} merkersList={merkersList} />
      </div>
    </div>

  );
}

export default App;
