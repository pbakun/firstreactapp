import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import { Marker,withScriptjs, withGoogleMap} from 'google-map-react';

const AnyReactComponent = ({ text }) => <div>{text}</div>
/*
export class MapContainter extends Component {

    MapDisplay() {
        return (
            <GoogleMapReact
                bootstrapURLKeys={{ key: 'AIzaSyBc-ZtnpZHSU3-4qdCOCU5IqkcUTID9c30' }}
                defaultCenter={{
                    lat: 54.360131,
                    lng: 18.645343
                }}
                defaultZoom={20}
              >
                <AnyReactComponent
                    lat={54.360131}
                    lng={18.645343}
                    text="Biuro Blunovation" />
            </GoogleMapReact>
        );
    }

    render() {
        return (
            <this.MapDisplay />
        );

    }console.log(props.merkersList);
            (props.merkersList).forEach(element => {
                
            });

}*/

const defaultCenter = {
    lat: 54.360131,
    lng: 18.645343
}

const MapContainter = props => {
    console.log(props.merkersList);
    return (
        <GoogleMapReact
            bootstrapURLKeys={{ key: 'AIzaSyBc-ZtnpZHSU3-4qdCOCU5IqkcUTID9c30' }}
            defaultCenter={defaultCenter}
            defaultZoom={20}
        >
            {props.merkersList.map((id, lat, lng, text) => {
                return (
                    <AnyReactComponent
                        key={id}
                        position={{ lat: lat, lng: lng }}
                        text={text} />
                );
            })}
        </GoogleMapReact>
    );
}
/*
const bla = () => {
<AnyReactComponent
                    lat={lat}
                    lng={lng}
                    text={text} />

<Marker key={props.merkersList[0].id} position={{lat:props.merkersList[0].lat, lng: props.merkersList[0].lng}}/>
         <AnyReactComponent
                    lat={54.360131}
                    lng={18.645343}
                    text="Biuro Blunovation" />

                    <AnyReactComponent
                    lat={54.360131}
                    lng={18.645343}
                    text="Biuro Blunovation" />

                    {props.merkersList.map((id, lat, lng, text) => {
                return(
                    <Marker
                    key={id}
                    position={{lat: lat, lng: lng}} />
                );
            })}
}*/

export default MapContainter;