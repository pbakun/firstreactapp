import React, { Component } from 'react';
import { Map, GoogleApiWrapper, Marker } from 'google-maps-react';


const defaultCenter = {
    lat: 54.360131,
    lng: 18.645343
}

const mapStyle = {
    width: '80%',
    height: '100%'
}

export class MapContainter extends Component {
    render() {
        return (
            <Map
                google={this.props.google}
                initialCenter={defaultCenter}
                style={mapStyle}
                zoom={20}
            >

                {this.props.merkersList.map((id, pos, text) => {
                    console.log(id)
                    return (
                        <Marker
                            key={id}
                            position={{pos}}
                            text={text} />
                    );


                })}
            </Map>
        );
    }

}

export default GoogleApiWrapper(
    (props) => ({
        apiKey: 'AIzaSyBc-ZtnpZHSU3-4qdCOCU5IqkcUTID9c30'
    }
    ))(MapContainter)